﻿using Pathfinding;
using UnityEngine;

public class AnimalManager : MonoBehaviour
{
    private RichAI richAI;
    private Seeker seeker;

    //-----------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------PRIVATE METHODS-------------------------------------------------------//
    //-----------------------------------------------------------------------------------------------------------------------------//
    private void Start()
    {
        seeker = GetComponent<Seeker>();
        richAI = GetComponent<RichAI>();

        richAI.maxSpeed = 3;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Vector3 destination = transform.position + Vector3.right * (Random.Range(0, 6) - 3) * 6 + Vector3.forward * (Random.Range(0, 6) - 3) * 6;
            Move(destination);
        }
    }

    private void Move(Vector3 destination)
    {
        seeker.StartPath(transform.position, destination);
    }
}
